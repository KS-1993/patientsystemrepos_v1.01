﻿using Sevices;
using Web;
using DataLayer;
using PatientSystem_v1._01.Context;
using System.Collections.Generic;
using System.Linq;



namespace PatientSystem_v1._01.Models
{
    public class PatientBussiness
    {
        private Data data = new Data();
        private Sevice service = new Sevice();
        private PatientContext db = new PatientContext();
        public List<Patient> View(string search)
        {
            string Search = service.CheckSearch(search);

            return db.Patients.Where(s=>s.FName.StartsWith(Search) ||
            s.LName.StartsWith(Search) ||
            s.Disease.StartsWith(Search) ||
            s.DOB.StartsWith(Search) ||
            Search == string.Empty).ToList();
        }
        public Patient Find(int? Id)
        {
            if (Id!=0)
            {
                return db.Patients.Find(Id);
            }
            else
            {
                return new Patient();
            }
        }
        public void Delete(Patient patient)
        {
            db.Patients.Remove(patient);
            db.SaveChanges();
        }
        public void AddOrEdit(Patient patient)
        {
            if(patient.PatientId == 0)
            {
                db.Patients.Add(patient);
                db.SaveChanges();
            }
            else
            {
                var Patient = db.Patients.Where(p => p.PatientId == patient.PatientId).FirstOrDefault();
                db.Patients.Remove(Patient);
                db.Patients.Add(patient);
                db.SaveChanges();
            }
        }
    }
}
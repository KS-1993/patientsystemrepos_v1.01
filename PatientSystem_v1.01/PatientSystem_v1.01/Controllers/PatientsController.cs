﻿using PatientSystem_v1._01.Models;
using System.Web.Mvc;
using PagedList;

namespace PatientSystem_v1._01.Controllers
{
    public class PatientsController : Controller
    {
        PatientBussiness patientBuseniss = new PatientBussiness();
        public ActionResult Index(int? i, string search)
       {
            return View(patientBuseniss.View(search).ToPagedList(i ?? 1,3));
        }
        public ActionResult Details(int Id)
        {
            return View(patientBuseniss.Find(Id));
        }
        public ActionResult Edit(int Id)
        {
            return View(patientBuseniss.Find(Id));
        }
        [HttpPost]
        public ActionResult Edit(Patient patient)
        {
            patientBuseniss.AddOrEdit(patient);
            return RedirectToAction("Index");
        }
        public ActionResult Delete(int? Id)
        {
            return View(patientBuseniss.Find(Id));
        }
        [HttpPost]
        public ActionResult Delete(int Id)
        {
            Patient patient = patientBuseniss.Find(Id);
            patientBuseniss.Delete(patient);
            return RedirectToAction("Index");
        }
    }
}